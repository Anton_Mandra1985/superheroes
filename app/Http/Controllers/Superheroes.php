<?php

namespace App\Http\Controllers;

use App\Superhero;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Superheroes extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $superheroesList = Superhero::select('id', 'nickname')
                ->orderBy('id', 'desc')
                ->paginate(5);

        foreach ($superheroesList as $item) {
            $path = DB::table('images')->select('path')
                    ->where('hero_id', $item->id)
                    ->first();
            $item->path = $path;
        }
        return view('layouts.index', ['data' => $superheroesList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('layouts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'nickname' => 'required|unique:superheroes,nickname',
            'realName' => 'required',
            'originDescription' => 'required',
            'superpowers' => 'required',
            'catchPhrase' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png',
        ]);
        $path = $request->image->store('uploads', 'public');
        $hero = new Superhero;
        $hero->nickname = $request->nickname;
        $hero->real_name = $request->realName;
        $hero->origin_description = $request->originDescription;
        $hero->superpowers = $request->superpowers;
        $hero->catch_phrase = $request->catchPhrase;
        $hero->save();

        DB::table('images')->insert(['hero_id' => $hero->id,
            'path' => $path]);
        return 'Hero was stored';
    }

    /**
     * Display the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $superhero = Superhero::where('id', $id)->get();
        if (count($superhero) === 0) {
            abort(404);
        }
        $images = DB::table('images')->select('path')
                ->where('hero_id', $id)
                ->get();
        return view('layouts.showHero', ['superhero' => $superhero[0],
            'images' => $images]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $superhero = Superhero::where('id', $id)->get();
        if (count($superhero) === 0) {
            abort(404);
        }
        $images = DB::table('images')->select('id', 'path')
                ->where('hero_id', $id)
                ->get();

        return view('layouts.edit', ['superhero' => $superhero[0],
            'images' => $images]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Superheroes  $superheroes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $this->validate($request, [
            'realName' => 'required',
            'originDescription' => 'required',
            'superpowers' => 'required',
            'catchPhrase' => 'required',
            'image' => 'mimes:jpeg,bmp,png',
        ]);

        $hero = Superhero::find($request->heroId);
        $hero->real_name = $request->realName;
        $hero->origin_description = $request->originDescription;
        $hero->superpowers = $request->superpowers;
        $hero->catch_phrase = $request->catchPhrase;
        $hero->save();

        if ($request->image) {
            $path = $request->image->store('uploads', 'public');
            DB::table('images')->insert(['hero_id' => $request->heroId,
                'path' => $path]);
        }
        
        return 'Hero was updated';
    }

    /**
     * Remove the specified resource from storage.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {

        $images = DB::table('images')->where('hero_id', $request->heroId)->get();

        foreach ($images as $image) {
            $disk = Storage::disk('public');
            $disk->delete($image->path);
        }

        DB::table('images')->where('hero_id', $request->heroId)->delete();
        Superhero::where('id', $request->heroId)->delete();
        return 'Hero was deleted';
    }

    /**
     * Remove the specified image from storage.
     *
     * 
     * @return \Illuminate\Http\Response
     */
    public function destroyImage(Request $request) {

        $image = DB::table('images')->where('id', $request->imageId)->get();
        $disk = Storage::disk('public');
        $disk->delete($image[0]->path);
        DB::table('images')->where('id', $request->imageId)->delete();
        return 'Image was deleted';
    }

}
