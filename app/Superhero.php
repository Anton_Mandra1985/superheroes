<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Superhero extends Model
{
    protected $table = 'superheroes';
    public $timestamps = false;
}
