<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Superheroes extends Model
{
    public $timestamps = false;
}
