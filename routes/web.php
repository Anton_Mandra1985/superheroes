<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Superheroes@index')->name('index');
Route::get('/show/{id}','Superheroes@show')->name('show');
Route::get('/create','Superheroes@create')->name('create');
Route::get('/edit/{id}','Superheroes@edit')->name('edit');
Route::post('/delete/{id}','Superheroes@destroy')->name('delete');
Route::post('/store','Superheroes@store')->name('store');
Route::post('/image_delete','Superheroes@destroyImage')->name('deleteImage');
Route::post('/update','Superheroes@update')->name('update');