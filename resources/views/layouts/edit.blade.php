@extends('layouts.app')
@section('title','Edit superhero')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>
                Edit {{$superhero->nickname}}
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 alert alert-danger" id="errors">
            <ul id="errors">
            </ul>
        </div>
    </div>
    <br>
    @if (count($images)!==0)
    <div class="row">
        @foreach($images as $image)
        <div id="{{$image->id}}" class="col-md-4">
            <img src="{{asset('/storage/'.$image->path)}}" alt="{{$superhero->nickname}}"
                 title="{{$superhero->nickname}}" width="150 px">
            <button  class="delete btn btn-danger" data-imageId="{{$image->id}}">Delete image</button>
        </div>
        @endforeach
    </div>
    @endif
</div>
<form>
    <div class="form-row">

        <div class="form-group col-md-12">
            <label for="real_name"><b>Real name:</b></label>
            <input type="text" class="form-control" id="real_name" 
                   name="real_name" value="{{$superhero->real_name}}">
        </div>

        <div class="form-group col-md-12">
            <label for="origin_description"><b>Origin description:</b></label>
            <textarea type="textarea" class="form-control" id="origin_description" 
                      name="origin_description">
                {{$superhero->origin_description}}
            </textarea>
        </div>

        <div class="form-group col-md-12">
            <label for="superpowers"><b>Superpowers:</b></label>
            <textarea type="textarea" class="form-control" id="superpowers" 
                      name="superpowers">
                {{$superhero->superpowers}}
            </textarea>
        </div>

        <div class="form-group col-md-12">
            <label for="catch_phrase"><b>Catch phrase:</b></label>
            <input type="text" class="form-control" id="catch_phrase" 
                   name="catch_phrase" value="{{$superhero->catch_phrase}}">
        </div>

        <div class="form-group col-md-12">
            <label for="image"><b>Add image:</b></label>
            <input type="file" class="form-control" id="image" name="image">
        </div>

    </div>
</form>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-info" id="editHero">Edit hero</button>
        </div>
    </div>
</div>
<script>
    $('document').ready(function () {

      
        $("#editHero").click(function () {
            $('li').remove();

            var formData = new FormData();
            $.each($('#image')[0].files, function (i, file) {
                formData.append('image', file);
            });
            formData.append('heroId', "{{$superhero->id}}");
            formData.append('realName', $("#real_name").val());
            formData.append('originDescription', $("#origin_description").val());
            formData.append('superpowers', $("#superpowers").val());
            formData.append('catchPhrase', $("#catch_phrase").val());

            $.ajax({
                type: "POST",
                url: "{{route('update')}}",
                data: formData,
                processData: false,
                contentType: false,
                success: function () {
                    $(location).attr("href", "{{route('show',$superhero->id)}}");
                },
                error: function (data) {
                    alert('There was some error performing the AJAX call!');
                    var errors = data.responseJSON.errors;
                    var error;
                    for (error in errors)
                    {
                        $('#errors').append("<li>" + error + " : " + errors[error] + "</li>");
                    }
                    $('#errors').show();
                }
            });
        });

        $(".delete").click(function () {
            var imageId = $(this).attr("data-imageId");
            $.ajax({
                type: "POST",
                url: "{{route('deleteImage')}}",
                data: {
                    imageId: imageId
                },
                success: function () {
                    var selector = "#" + imageId;
                    $(selector).remove();
                }
            });
        });
    });



</script>
<script src="{{url('js/hideAjaxErrors.js')}}"></script>
@endsection