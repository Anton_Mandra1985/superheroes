@extends('layouts.app')
@section('title',$superhero->real_name)
@section('content')
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog alert-danger" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete hero?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button id="confirmDelete" type="button" class="btn btn-danger" data-dismiss="modal"  data-target="#deleteModal">Confirm</button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <b>Nickname:</b> {{$superhero->nickname}}
    </div>
    <div class="col-sm-12">
        <b>Real name:</b> {{$superhero->real_name}}
    </div>
    <div class="col-sm-12">
        <b>Origin description:</b> {{$superhero->origin_description}}
    </div>
    <div class="col-sm-12">
        <b>Superpowers:</b> {{$superhero->superpowers}}
    </div>
    <div class="col-sm-12">
        <b>Catch phrase:</b> {{$superhero->catch_phrase}}
    </div>
</div    >
<div class="row">
    @if (count($images)===0)
    <div class="col-md-12">
        <b>No image</b>
    </div>

    @else
    @foreach ($images as $image)
    <div class="row">
        <div class="col-md-4">

            <img src="{{asset('/storage/'.$image->path)}}" alt="{{$superhero->nickname}}"
                 title="{{$superhero->nickname}}" width="300 px">

        </div>
    </div>
    @endforeach
    @endif
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <a  href="{{route('edit',$superhero->id)}}" class = "btn btn-info">Edit</a>
        <button class = "btn btn-danger" data-heroId="{{$superhero->id}}" 
                data-target="#deleteModal" data-toggle="modal"
                data-url="{{route('delete',$superhero->id)}}">
            Delete
        </button>
    </div>
</div>

<script>
    $('document').ready(function () {
        var heroId;
        var delUrl;
    
        $('[data-heroId]').click(function () {
            heroId = getHeroId($(this));
            delUrl = getUrl($(this));
        });

        $('#confirmDelete').click(function () {
            $.ajax({
                type: "POST",
                url: delUrl,

                data: {heroId: heroId},
                success: function () {
                    $(location).attr("href", "{{route('index')}}");

                },
                error: function () {
                    alert('There was some error performing the AJAX call!');
                }
            });
        });
    });

</script>
<script src="{{url('js/functions/getHeroId.js')}}"></script>
<script src="{{url('js/functions/getUrl.js')}}"></script>
@endsection



