@extends('layouts.app')
@section('title','Create superhero')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>
                Create new hero
            </h1>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 alert alert-danger" id="errors">
        <ul id="errors">
        </ul>
    </div>
</div>
<form action="" enctype="multipart/form-data">
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="nickname"><b>Nickname:</b></label>
            <input type="text" class="form-control" id="nickname" name="nickname" required>
        </div>

        <div class="form-group col-md-12">
            <label for="real_name"><b>Real name:</b></label>
            <input type="text" class="form-control" id="real_name" name="real_name" required>
        </div>

        <div class="form-group col-md-12">
            <label for="origin_description"><b>Origin description:</b></label>
            <textarea type="textarea" class="form-control" id="origin_description" name="origin_description" required>
            </textarea>
        </div>

        <div class="form-group col-md-12">
            <label for="superpowers"><b>Superpowers:</b></label>
            <textarea type="textarea" class="form-control" id="superpowers" name="superpowers" required>
            </textarea>
        </div>

        <div class="form-group col-md-12">
            <label for="catch_phrase"><b>Catch phrase:</b></label>
            <input type="text" class="form-control" id="catch_phrase" name="catch_phrase" required>
        </div>

        <div class="form-group col-md-12">
            <label for="image"><b>Image:</b></label>
            <input type="file" class="form-control" id="image" name="image">
        </div>

    </div>
</form>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-success" id="addHero">Add hero</button>
        </div>
    </div>
</div>
<script>
    $('document').ready(function () {
      
        $("#addHero").click(function () {

            $('li').remove();

            var formData = new FormData();
            $.each($('#image')[0].files, function (i, file) {
                formData.append('image', file);
            });
            formData.append('nickname', $("#nickname").val());
            formData.append('realName', $("#real_name").val());
            formData.append('originDescription', $("#origin_description").val());
            formData.append('superpowers', $("#superpowers").val());
            formData.append('catchPhrase', $("#catch_phrase").val());

            $.ajax({
                type: "POST",
                url: "{{route('store')}}",
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $(location).attr("href", "{{route('index')}}");
                    console.log(data);

                },
                error: function (data) {
                    var errors = data.responseJSON.errors;
                    var error;
                    for (error in errors) {
                        $('#errors').append("<li>" + error + " : " + errors[error] + "</li>");
                    }

                    $('#errors').show();
                }
            });

        });
    });


</script>
<script src="{{url('js/hideAjaxErrors.js')}}"></script>
@endsection