@extends('layouts.app')
@section('title','Superheroes list')
@section('content')
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog alert-danger" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete hero?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button id="confirmDelete" type="button" class="btn btn-danger" data-dismiss="modal"  data-target="#deleteModal">Confirm</button>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <h1>Superheroes list</h1>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <a  href="{{route('create')}}" class = "btn btn-success">Add hero</a>
    </div>
</div>
<br>
<div class="row">
    <div class="col-sm-12">
        <table class="table">
            <tr>
                <th>NickName</th>
                <th>Photo</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            @foreach($data as $superhero)
            <tr id= "{{$superhero->id}}" class = "text-center">
                <td>
                    <a href="{{{route('show',$superhero->id)}}}">
                        {{$superhero->nickname}}
                    </a>
                </td>
                <td>@if ($superhero->path==NULL)
                    <b>No image</b>
                    @else
                    <img src="{{asset('/storage/'.$superhero->path->path)}}" alt="{{$superhero->nickname}}"
                         title="{{$superhero->nickname}}" width="50 px">
                    @endif
                </td>
                <td><a  href="{{route('edit',$superhero->id)}}" class = "btn btn-info">Edit</a></td>
                <td>
                    <button class = "btn btn-danger" data-heroId="{{$superhero->id}}" 
                            data-target="#deleteModal" data-toggle="modal"
                            data-url="{{route('delete',$superhero->id)}}">
                        Delete
                    </button>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
    <div class="row">
        {{ $data->links() }}
    </div>
</div>

<script src="{{url('js/indexDeleteHero.js')}}"></script>
<script src="{{url('js/functions/getHeroId.js')}}"></script>
<script src="{{url('js/functions/getUrl.js')}}"></script>
@endsection