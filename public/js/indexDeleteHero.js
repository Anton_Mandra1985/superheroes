$('document').ready(function () {
        var heroId;
        var delUrl;        

        $('[data-heroId]').click(function () {
            heroId = getHeroId($(this));
            delUrl = getUrl($(this));
        });

        $('#confirmDelete').click(function () {
            $.ajax({
                type: "POST",
                url: delUrl,

                data: {heroId: heroId},
                success: function () {
                    var selector = '#' + heroId;
                    $(selector).remove();

                },
                error: function () {
                    alert('There was some error performing the AJAX call!');
                }
            });
        });
    });